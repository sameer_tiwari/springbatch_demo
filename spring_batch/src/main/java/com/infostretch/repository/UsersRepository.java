package com.infostretch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infostretch.entity.Users;

public interface UsersRepository extends JpaRepository<Users, Long> {
}
